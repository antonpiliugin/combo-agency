import { Component, Input } from '@angular/core';
import { ItemComponent } from '../item/item.component';
import { Task } from '../app.component';

@Component({
  selector: 'app-top-left-selected',
  standalone: true,
  imports: [ItemComponent],
  templateUrl: './top-left-selected.component.html',
  styleUrl: './top-left-selected.component.scss'
})
export class TopLeftSelectedComponent {
  @Input() total: number = 0;
  @Input() data: Task[] = [];
}
