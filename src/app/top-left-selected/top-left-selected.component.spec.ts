import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopLeftSelectedComponent } from './top-left-selected.component';

describe('TopLeftSelectedComponent', () => {
  let component: TopLeftSelectedComponent;
  let fixture: ComponentFixture<TopLeftSelectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TopLeftSelectedComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TopLeftSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
