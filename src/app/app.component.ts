import { Component, computed } from '@angular/core';
import { TopLeftSelectedComponent } from './top-left-selected/top-left-selected.component';
import { TopRightSelectedComponent } from './top-right-selected/top-right-selected.component';
import { ItemsListComponent } from './items-list/items-list.component';
export interface Task {
  id: number;
  name: string;
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    TopLeftSelectedComponent,
    TopRightSelectedComponent,
    ItemsListComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'test-task'
  leftSelected: Task[] = []
  rightSelected: Task | null = null;
  left: Task[] = [
    {
      "id": 1,
      "name": "Shoes 1"
    },
    {
      "id": 2,
      "name": "Shoes 2"
    },
    {
      "id": 3,
      "name": "Shoes 3"
    },
    {
      "id": 4,
      "name": "Shoes 4"
    },
    {
      "id": 5,
      "name": "T-shirt 1"
    },
    {
      "id": 6,
      "name": "T-shirt 2"
    },
    {
      "id": 7,
      "name": "T-shirt 3"
    },
    {
      "id": 8,
      "name": "T-shirt 4"
    }
  ]

  right: Task[] = [
    {
      "id": 11,
      "name": "Jacket 1"
    },
    {
      "id": 12,
      "name": "Jacket 2"
    },
    {
      "id": 13,
      "name": "Jacket 3"
    },
    {
      "id": 14,
      "name": "Jacket 4"
    },
    {
      "id": 15,
      "name": "Hoodie 1"
    },
    {
      "id": 16,
      "name": "Hoodie 2"
    },
    {
      "id": 17,
      "name": "Hoodie 3"
    },
    {
      "id": 18,
      "name": "Hoodie 4"
    }
  ]

  selectedLeft(taskId: number) {
    const item = this.left.find((item) => item.id === taskId)
    if (!item) return

    const foundItemIndex = this.leftSelected.findIndex((item) => item.id === taskId)
    if (foundItemIndex > -1) {
      this.leftSelected.splice(foundItemIndex, 1)
    } else if (this.leftSelected.length < 6) {
      this.leftSelected.push(item)
    }
  }

  selectedRight(taskId: number) {
    const item = this.right.find((item) => item.id === taskId)
    if (!item) return

    if (this.rightSelected?.id === taskId) {
      this.rightSelected = null
    } else {
      this.rightSelected = item
    }
  }
}
