import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ItemComponent } from '../item/item.component';
import { Task } from '../app.component';

@Component({
  selector: 'app-items-list',
  standalone: true,
  imports: [ItemComponent],
  templateUrl: './items-list.component.html',
  styleUrl: './items-list.component.scss'
})

export class ItemsListComponent {
  @Input() data: Task[] = []
  @Output() selectedItem = new EventEmitter<number>()

  selected(taskId: number) {
    this.selectedItem.emit(taskId)
  }
}
