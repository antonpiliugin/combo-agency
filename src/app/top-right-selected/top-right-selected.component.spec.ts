import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopRightSelectedComponent } from './top-right-selected.component';

describe('TopRightSelectedComponent', () => {
  let component: TopRightSelectedComponent;
  let fixture: ComponentFixture<TopRightSelectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TopRightSelectedComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TopRightSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
