import { Component, Input } from '@angular/core';
import { Task } from '../app.component';

@Component({
  selector: 'app-top-right-selected',
  standalone: true,
  imports: [],
  templateUrl: './top-right-selected.component.html',
  styleUrl: './top-right-selected.component.scss'
})
export class TopRightSelectedComponent {
  @Input() item!: Task | null;
}
