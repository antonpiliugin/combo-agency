import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Task } from '../app.component';

@Component({
  selector: 'app-item',
  standalone: true,
  imports: [],
  templateUrl: './item.component.html',
  styleUrl: './item.component.scss'
})
export class ItemComponent {
  @Input() task!: Task
  @Output() selected = new EventEmitter<number>()

  select(taskId: number) {
    this.selected.emit(taskId)
  }
}
